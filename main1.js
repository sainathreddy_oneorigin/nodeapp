//Change the file location if required in 103,142,184
const axios = require('axios').default;
const fs = require('fs');
var stringify = require('csv-stringify');
const { type } = require('os');
const jsonexport = require('jsonexport');
var readline = require('readline-sync');



//Username of the account and passord 
var user = readline.question("Enter Username : ");
var password = readline.question("Enter Password : ");
//Workspace Name 
var workspace = readline.question("Enter Workspace : ");


//Function to get the memberLink
function memberLink() {
    axios.get('https://api.bitbucket.org/2.0/teams/' + workspace, {
        auth: {
            username: user,
            password: password
        }
    })
        .then(resp => {
            var json = resp.data;
            var obj = JSON.parse(JSON.stringify(json));
            var workspace_id = obj.uuid;
            //userGroup Function to get
            userGroup(workspace_id);
            var linksArr = obj.links.members.href;
            var link = linksArr;
            var headers = "UserName, RepoName \n";
            fs.writeFile('data/repo.csv', headers, function (err) {
                if (err) return console.error(err);
            });
            //calling the memberName_repo Function with the member URl to get name and repo URL
            memberName_repo(linksArr);
        })
        .catch(err => {
            // Handle Error Here
            console.error(err);
        });
}

//memberName_repo Function used to get the member name the url repo of the URL
function memberName_repo(linksArr) {
    axios.get(linksArr, {
        auth: {
            username: user,
            password: password
        }
    })
        .then(resp => {
            const json1 = resp.data;
            const obj1 = JSON.parse(JSON.stringify(json1));
            var members = obj1.values;
            var name = new Array("");
            var size = obj1.size;
            var i = 0;
            var nameListinWorspace = new Array();
            for (let val of members) {
                var name = val.display_name;
                var repo = val.links.repositories.href;
                //Function to get repo names with the member name 
                reoName(repo, name);
                nameListinWorspace[i++] = name;
            }
            //Function to get the Access type and name of the user 
            permissionMode();
        })
        .catch(err => {
            // Handle Error Here
            console.error(err);
        });
}

//Function to get the repo names 
function reoName(repoUrl, name) {
    axios.get(repoUrl, {
        auth: {
            username: user,
            password: password
        }
    })
        .then(resp => {
            const json2 = resp.data;
            const obj2 = JSON.parse(JSON.stringify(json2));
            var repoDet = obj2.values;
            var jsonArray = [];
            var accessName = new Array();
            var accessType = new Array();
            for (let val of repoDet) {
                var repoName = val.slug;
                var jsonObj = {};
                var line = name +","+repoName+"\n";
                fs.appendFile('data/repo.csv', line, function (err) {
                    if (err) return console.error(err);
                });
            }
            if (repoDet == '') {
                var jsonObj = {};
                var repoName = 'No Repository';
                var line = name +","+repoName+"\n";
                fs.appendFile('data/repo.csv', line, function (err) {
                    if (err) return console.error(err);
                });
            }
        })
        .catch(err => {
            // Handle Error Here
            console.error(err);
        });
}

//Function to get the Access type and name of the user 
function permissionMode() {
    axios.get('https://api.bitbucket.org/2.0/teams/' + workspace + '/permissions/repositories', {
        auth: {
            username: user,
            password: password
        }
    })
        .then(resp => {
            const json3 = resp.data;
            const obj3 = JSON.parse(JSON.stringify(json3));
            var repoDet = obj3.values;
            var i = 0;
            var j = 0;

            var jsonArray = [];
            var accessName = new Array();
            var accessType = new Array();
            for (let val of repoDet) {
                var name = val.user.display_name;
                var access_type = val.permission;
                var jsonObj = {};
                jsonObj.Name = name;
                jsonObj.Permission = access_type;
                jsonArray.push(jsonObj);
            }
            jsonexport(jsonArray, function (err, csv) {
                if (err) return console.error(err);
                fs.writeFile('data/user_access.csv', csv, function (err) {
                    if (err) return console.error(err);
                });
            });

        })
        .catch(err => {
            // Handle Error Here
            console.error(err);
        });
}
//userGroup Function used to get the user group and the member in it 
function userGroup(workspace_id) {
    axios.get('https://api.bitbucket.org/1.0/groups/' + workspace_id, {
        auth: {
            username: user,
            password: password
        }
    })
        .then(resp => {
            var respData = resp.data;
            var jsonArray = [];
            for (i in respData) {
                var userGroup = respData[i];
                var groupName = userGroup.name;
                var members = userGroup.members;
                for (j in members) {
                    var member = members[j];
                    var jsonObj = {};
                    jsonObj.GroupName = groupName;
                    jsonObj.MemberName = member.display_name;
                    jsonArray.push(jsonObj);
                }
                if (members.length == 0) {
                    var jsonObj = {};
                    jsonObj.GroupName = groupName;
                    jsonObj.MemberName = "No User";
                    jsonArray.push(jsonObj);
                }
            }
            jsonexport(jsonArray, function (err, csv) {
                if (err) return console.error(err);
                fs.writeFile('data/user_group.csv', csv, function (err) {
                    if (err) return console.error(err);
                });
            });
        })
        .catch(err => {
            // Handle Error Here
            console.error(err);
        });

}
memberLink();